package it.polito.dp2.WF.sol3;

import it.polito.dp2.WF.ProcessActionReader;
import it.polito.dp2.WF.WorkflowReader;

public class ProcessAction extends Action implements ProcessActionReader {
	private WorkflowReader actionWorkflow;

	public ProcessAction(String name, String role, WorkflowReader enclosingWorkflow, boolean isAutomaticallyInstantiated) {
		super(name, role, enclosingWorkflow, isAutomaticallyInstantiated);
	}

	@Override
	public WorkflowReader getActionWorkflow() {
		return actionWorkflow;
	}

	public void setActionWorkflow(WorkflowReader actionWorkflow) {
		this.actionWorkflow = actionWorkflow;
	}
	
	
}
