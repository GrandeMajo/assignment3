package it.polito.dp2.WF.sol3;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;

import it.polito.dp2.WF.WorkflowMonitorException;
import it.polito.dp2.WF.WorkflowReader;
import it.polito.dp2.WF.lab3.gen.Workflow;
import it.polito.dp2.WF.lab3.gen.Action;
import it.polito.dp2.WF.lab3.gen.UnknownNames_Exception;
import it.polito.dp2.WF.lab3.gen.WorkflowInfo;
import it.polito.dp2.WF.lab3.gen.WorkflowInfoService;

import it.polito.dp2.WF.sol3.ProcessAction;
import it.polito.dp2.WF.sol3.Resources;
import it.polito.dp2.WF.sol3.WorkflowMonitorFactory;
import it.polito.dp2.WF.sol3.WorkflowMonitorImplementation;
import it.polito.dp2.WF.sol3.SimpleAction;

public class WorkflowMonitorClient {
	private static final int ENCLOSING_WORKFLOW_INDEX	= 0; 
	private static final int ACTION_WORKFLOW_INDEX		= 1; 
	private static final int PROCESS_ACTION_INDEX 		= 2;
//	private static final int BINDING_PORT		 		= 8888;
	
	private static final Logger logger = Logger.getLogger(WorkflowMonitorClient.class.getName());
	
	private String url;
	private WorkflowMonitorFactory workflowMonitorFactory;
	private WorkflowMonitorImplementation monitor;
	private HashMap<String, ArrayList<String>> unsolvedNextActions;
	private LinkedList<String[]> unsolvedActionWorkflows;
	private XMLGregorianCalendar lastUpdate;

	public WorkflowMonitorClient(String url) throws WorkflowMonitorException {
		Resources.setLogHandler(logger, false);
		try {
			this.url = url;
			workflowMonitorFactory = new WorkflowMonitorFactory();
			monitor = (WorkflowMonitorImplementation) workflowMonitorFactory.newWorkflowMonitor();
			unsolvedNextActions = new HashMap<String, ArrayList<String>>(); 
			unsolvedActionWorkflows = new LinkedList<String[]>();
		} catch (WorkflowMonitorException wme) {
			logger.severe(wme.getMessage());
			throw wme;
		} catch (Exception e) {
			logger.severe(e.getMessage());
			throw new WorkflowMonitorException(e);
		}
	}

	public WorkflowMonitorClient(WorkflowMonitorImplementation monitor, String url) throws WorkflowMonitorException {
		Resources.setLogHandler(logger, false);
		try {
			this.url = url;
			this.monitor = monitor;
			unsolvedNextActions = new HashMap<String, ArrayList<String>>();
			unsolvedActionWorkflows = new LinkedList<String[]>();
		} catch (Exception e) {
			throw new WorkflowMonitorException(e);
		}
	}
	
	public void service() throws WorkflowMonitorException {
		logger.info("Starting Client...");
		WorkflowInfoService infoService;
		try {
			infoService = (url == null)?
					new WorkflowInfoService() :
						new WorkflowInfoService(new URL(url));				
			
//			URL serviceUrl = infoService.getWSDLDocumentLocation();
//			URL bindingUrl = new URL(serviceUrl.getProtocol(), serviceUrl.getHost(), BINDING_PORT, serviceUrl.getFile());

			WorkflowInfo infoProxy = infoService.getWorkflowInfoPort();
//			BindingProvider provider = (BindingProvider) infoProxy;
//			provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, bindingUrl.toExternalForm());
			
//			WorkflowSetService setService = new WorkflowSetService();
//			WorkflowSet setProxy = setService.getWorkflowSetPort();
			
			Holder<XMLGregorianCalendar> calendarHolder = new Holder<XMLGregorianCalendar>();
			Holder<List<String>> namesHolder = new Holder<List<String>>();
			
			logger.info("Getting workflow names...");

			infoProxy.getWorkflowNames(calendarHolder, namesHolder);
			List<String> workflowNames = namesHolder.value;
			
			if (lastUpdate != null && lastUpdate.equals(calendarHolder.value)) {
				logger.info("Info already updated, nothing to request");
				return;
			}
			
			logger.info("workflows available (" + workflowNames.size() + "): " + workflowNames);
			
			if (workflowNames.isEmpty())
				return;
			
			logger.info("Getting workflows...");
			
			Holder<List<Workflow>> workflowsHolder = new Holder<List<Workflow>>();			
			infoProxy.getWorkflows(workflowNames, calendarHolder, workflowsHolder);
						
			monitor.clearWorkflows();
			monitor.clearProcesses();
			
			List<Workflow> workflows = workflowsHolder.value;
			
			logger.info("got " + workflows.size() + " workflows: " + workflows);
			
			for (Workflow workflow : workflows)
				monitor.addWorkflow(getWorkflow(workflow));
			
			solveActionWorkflows();
			
			lastUpdate = calendarHolder.value;	
			
//			ArrayList<it.polito.dp2.WF.lab3.gen.set.Workflow> workflows2 = new ArrayList<it.polito.dp2.WF.lab3.gen.set.Workflow>();
//			it.polito.dp2.WF.lab3.gen.set.Workflow workflow2 = new it.polito.dp2.WF.lab3.gen.set.Workflow();
//			for (Workflow workflow : workflows) {
//				copyWorkflow(workflow, workflow2);
//				workflows2.add(workflow2);
//			}
//			XMLGregorianCalendar resultCalendar = setProxy.setWorkflows(workflows2);
//			System.out.println(resultCalendar.toGregorianCalendar());
			
		} catch (WorkflowMonitorException wme) {
			logger.severe(wme.getMessage());
			throw wme;
		} catch (UnknownNames_Exception une) {
			logger.severe(une.getMessage() + "\nThe unknown names are: " + une.getFaultInfo().getNames());
			throw new WorkflowMonitorException(une);
		} catch (Exception e) {
			logger.severe(e.getMessage());
			throw new WorkflowMonitorException(e);
		} finally {
			logger.info("Client terminated");			
		}
	}

	private it.polito.dp2.WF.sol3.Workflow getWorkflow(Workflow workflow) throws WorkflowMonitorException {
		String name = workflow.getName();
		if (name == null || name.isEmpty())
			throw new WorkflowMonitorException("Workflow name attribute is null or empty.");
		if (!name.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid workflow name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		
		it.polito.dp2.WF.sol3.Workflow myWorkflow = new it.polito.dp2.WF.sol3.Workflow(name);
		
		List<Action> actions = workflow.getAction();
		for (Action action : actions)
			myWorkflow.addAction(getAction(action, myWorkflow));
		
		for (Entry<String, ArrayList<String>> entry : unsolvedNextActions.entrySet()) {
	    	SimpleAction action = (SimpleAction) myWorkflow.getAction(entry.getKey());
	    	ArrayList<String> nextActions = entry.getValue();
	    	for (String nextAction : nextActions) {
	    		if (!nextAction.matches(Resources.NAME_REGEX))
	    			throw new WorkflowMonitorException("Invalid nextAction name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
	    		
	    		it.polito.dp2.WF.sol3.Action a = (it.polito.dp2.WF.sol3.Action) myWorkflow.getAction(nextAction);
	    		if (a == null)
	    			throw new WorkflowMonitorException("Invalid name for next action " + nextAction + ", action not exists.");
	    		
	    		action.addPossibleNextAction(a);
			}
	    }
		unsolvedNextActions.clear();
		
		return myWorkflow;
	}
	
	public it.polito.dp2.WF.sol3.Action getAction(Action action, WorkflowReader enclosingWorkflow) throws WorkflowMonitorException {
		String name = action.getName();
		if (name == null || name.isEmpty())
			throw new WorkflowMonitorException("Action name attribute is null or empty.");
		if (!name.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid action name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");

		String role = action.getRole();
		if (role == null || role.isEmpty())
			throw new WorkflowMonitorException("Action role attribute is null or empty.");
		if (!role.matches(Resources.ROLE_REGEX))
			throw new WorkflowMonitorException("Invalid role name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");

		String actionWorkflowName = action.getWorkflow();	
		if (actionWorkflowName == null) {
			SimpleAction simpleAction = new SimpleAction(name, role, enclosingWorkflow, action.isAutomaticallyInstantiated());
			unsolvedNextActions.put(name, (ArrayList<String>) action.getNextAction());
			return simpleAction;
		} else {
			if (!actionWorkflowName.matches(Resources.NAME_REGEX))
				throw new WorkflowMonitorException("Invalid action actionWorkflow name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
			
			ProcessAction processAction = new ProcessAction(name, role, enclosingWorkflow, action.isAutomaticallyInstantiated());
			unsolvedActionWorkflows.add(new String[] { enclosingWorkflow.getName(), actionWorkflowName, name });
			return processAction;
		}
	}	
	
	private void solveActionWorkflows() throws WorkflowMonitorException {
		for (String[] unsolved : unsolvedActionWorkflows) {
			it.polito.dp2.WF.sol3.Workflow actionWorkflow = (it.polito.dp2.WF.sol3.Workflow) monitor.getWorkflow(unsolved[ACTION_WORKFLOW_INDEX]);			
			if (actionWorkflow == null)
				throw new WorkflowMonitorException("Invalid " + Resources.ACTION_WORKFLOW + " name, it not exists.");
			
			it.polito.dp2.WF.sol3.Workflow enclosingWorkflow = (it.polito.dp2.WF.sol3.Workflow) monitor.getWorkflow(unsolved[ENCLOSING_WORKFLOW_INDEX]);
			ProcessAction processAction = (ProcessAction) enclosingWorkflow.getAction(unsolved[PROCESS_ACTION_INDEX]);
			processAction.setActionWorkflow(actionWorkflow);
		}
		unsolvedActionWorkflows.clear();
	}	
		
//	private static void copyWorkflow(Workflow workflow1, it.polito.dp2.WF.lab3.gen.set.Workflow workflow2) {
//        workflow2.setName(workflow1.getName());
//        List<it.polito.dp2.WF.lab3.gen.set.Action> actions = workflow2.getAction();
//        actions.clear();
//        for (it.polito.dp2.WF.lab3.gen.Action as : workflow1.getAction()) {
//            it.polito.dp2.WF.lab3.gen.set.Action a = new it.polito.dp2.WF.lab3.gen.set.Action();
//            copyAction(as, a);
//            actions.add(a);
//        }
//    }
//
//    private static void copyAction(it.polito.dp2.WF.lab3.gen.Action action1, it.polito.dp2.WF.lab3.gen.set.Action action2) {
//        action2.setName(action1.getName());
//        action2.setRole(action1.getRole());
//        action2.setAutomaticallyInstantiated(action1.isAutomaticallyInstantiated());
//        action2.setWorkflow(action1.getWorkflow());
//        List<String> actions = action2.getNextAction();
//        actions.clear();
//        for (String s : action1.getNextAction()) {
//            actions.add(s);
//        }
//    }
}
