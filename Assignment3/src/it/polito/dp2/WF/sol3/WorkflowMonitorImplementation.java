package it.polito.dp2.WF.sol3;

import java.util.LinkedList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import it.polito.dp2.WF.ProcessReader;
import it.polito.dp2.WF.WorkflowMonitor;
import it.polito.dp2.WF.WorkflowMonitorException;
import it.polito.dp2.WF.WorkflowReader;
import it.polito.dp2.WF.lab3.Refreshable;

public class WorkflowMonitorImplementation implements WorkflowMonitor, Refreshable {
	
	private static final String PROPERTY_URL = "it.polito.dp2.WF.sol3.URL";
	
	private HashMap<String, Workflow> workflows;
    private LinkedList<Process> processes;
    private WorkflowMonitorClient client;
    
	public WorkflowMonitorImplementation() throws WorkflowMonitorException {
		workflows = new HashMap<String, Workflow>();
		processes = new LinkedList<Process>();
		
		try {
			String url = System.getProperty(PROPERTY_URL);
			client = new WorkflowMonitorClient(this, url);
			client.service();
		}catch (WorkflowMonitorException wme) {
			throw wme;
		} catch (Exception e) {
			throw new WorkflowMonitorException(e);
		}
	}

	@Override
	public Set<ProcessReader> getProcesses() {
		return new LinkedHashSet<ProcessReader>(processes);
	}

	@Override
	public WorkflowReader getWorkflow(String workflowName) {
		return workflows.get(workflowName);
	}

	@Override
	public Set<WorkflowReader> getWorkflows() {
		return new LinkedHashSet<WorkflowReader>(workflows.values());
	}

	public void addWorkflow(Workflow workflow) {
		workflows.put(workflow.getName(), workflow);
	}
	
	public void addProcess(Process process) {
		processes.add(process);
	}
	
	public void clearWorkflows() {
		workflows.clear();
	}

	public void clearProcesses() {
		processes.clear();
	}

	@Override
	public void refresh() {
		try {
			if (client == null) {
				String url = System.getProperty(PROPERTY_URL);
				client = new WorkflowMonitorClient(this, url);
			}
			client.service();
		} catch (WorkflowMonitorException e) {
			e.printStackTrace();
		} 
	}
}
