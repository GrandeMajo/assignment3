package it.polito.dp2.WF.sol3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import it.polito.dp2.WF.ActionReader;
import it.polito.dp2.WF.SimpleActionReader;
import it.polito.dp2.WF.WorkflowReader;

public class SimpleAction extends Action implements SimpleActionReader {
	private ArrayList<ActionReader> possibleNextActions;

	public SimpleAction(String name, String role, WorkflowReader enclosingWorkflow, boolean isAutomaticallyInstantiated) {
		super(name, role, enclosingWorkflow, isAutomaticallyInstantiated);
		possibleNextActions = new ArrayList<ActionReader>();
	}

	@Override
	public Set<ActionReader> getPossibleNextActions() {
		return new LinkedHashSet<ActionReader>(possibleNextActions);
	}
	
	public void addPossibleNextAction(ActionReader action) {
		possibleNextActions.add(action);
	}

	public void addPossibleNextActions(Collection<? extends ActionReader> actions) {
		possibleNextActions.addAll(actions);
	}
}
