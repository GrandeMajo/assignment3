package it.polito.dp2.WF.sol3;

import java.util.Calendar;

import it.polito.dp2.WF.ActionStatusReader;
import it.polito.dp2.WF.Actor;

public class ActionStatus implements ActionStatusReader {
	private String actionName;
    private Actor actor;
    private Calendar terminationTime;
    
	public ActionStatus(String actionName, Actor actor, Calendar terminationTime) {
		this.actionName = actionName;
		this.actor = actor;
		this.terminationTime = terminationTime;
	}

	@Override
	public String getActionName() {
		return actionName;
	}

	@Override
	public Actor getActor() {
		return actor;
	}

	@Override
	public Calendar getTerminationTime() {
		return terminationTime;
	}

	@Override
	public boolean isTakenInCharge() {
		return actor != null;
	}

	@Override
	public boolean isTerminated() {
		return terminationTime != null;
	}

}
