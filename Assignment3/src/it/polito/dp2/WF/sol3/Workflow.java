package it.polito.dp2.WF.sol3;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import it.polito.dp2.WF.ActionReader;
import it.polito.dp2.WF.ProcessReader;
import it.polito.dp2.WF.WorkflowReader;

public class Workflow implements WorkflowReader {
	private String name;
    private HashMap<String, Action> actions;
    private LinkedList<Process> processes;

	public Workflow(String name) {
		this.name = name;
		actions = new HashMap<String, Action>();
		processes = new LinkedList<Process>();
	}

	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public ActionReader getAction(String actionName) {
		return actions.get(actionName);
	}

	@Override
	public Set<ActionReader> getActions() {
		return new HashSet<ActionReader>(actions.values());
	}

	@Override
	public Set<ProcessReader> getProcesses() {
		return new HashSet<ProcessReader>(processes);
	}

	public void addAction(Action action) {
		actions.put(action.getName(), action);
	}
	
	public void addProcess(Process process) {
		processes.add(process);
	}
}
