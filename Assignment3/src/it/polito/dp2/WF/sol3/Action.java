package it.polito.dp2.WF.sol3;

import it.polito.dp2.WF.ActionReader;
import it.polito.dp2.WF.WorkflowReader;

public class Action implements ActionReader {
	private String name;
	private String role;
    private WorkflowReader enclosingWorkflow;
    private boolean isAutomaticallyInstantiated;

	public Action(String name, String role, WorkflowReader enclosingWorkflow, boolean isAutomaticallyInstantiated) {
		this.name = name;
		this.role = role;
		this.enclosingWorkflow = enclosingWorkflow;
		this.isAutomaticallyInstantiated = isAutomaticallyInstantiated;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getRole() {
		return role;
	}

	@Override
	public WorkflowReader getEnclosingWorkflow() {
		return enclosingWorkflow;
	}
	
	@Override
	public boolean isAutomaticallyInstantiated() {
		return isAutomaticallyInstantiated;
	}

}
